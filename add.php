<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div class="off-canvas-wrapper">
		<?php require_once('tpl/header.php');	?>

		<main class="container off-canvas-content" data-off-canvas-content>
			<div class="row">
				<h1 class="page-title">Ajouter une tâche</h1>
				<form method="post" action="update.php" class="small-12 medium-6 collumn">
					<label>Description</label>
					<textarea name="description" rows="6"></textarea>
					<label>Priorité</label>
					<select name="priority">
						<?php for($i = 1; $i <= 5; $i++): ?>
							<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php endfor; ?>
					</select>
					<label>Délai</label>
					<input type="date" name="due_at"/>
					<label>Assignée à</label>
					<select name="assigned_to">
						<?php
						$query = $db -> query('SELECT * FROM user');
						while($data =	$query -> fetch()):
							?>
						<option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
						<?php
						endwhile;
						?>
					</select>
					<input type="submit" value="Ajouter" class="button"/>
				</form>
			</div>
		</main>

		<?php require_once('tpl/footer.php'); ?>
	</div>
</body>
</html>
