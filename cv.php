<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div>
		<?php require_once('tpl/header.php'); ?>
		<main class="container off-canvas-content" data-off-canvas-content>
			<body>
				<u>
             <h1>Curriculum vitae</h1>
          </u>
          <br>
          <br>
          <br>
          <table>
            <tr>
            </tr>
            <tr>
              <th>Nom</th>
              <td>Reynaud</td>
              <td rowspan="4"><img image src="css/img/imagecv.jpg" width="150"></td>
           </tr>
           <tr>
              <th>Prenom</th>
              <td>Mickael</td>
           </tr>
           <tr>
              <th>Date de naissance</th>
              <td>4 octobre 1995</td>
           </tr>
           <tr>
              <th>Adresse</th>
              <td>Chemin du Palatinat 13</td>
           </tr>
           <tr>
              <th>E-mail</th>
              <td><a href="mailto: mickael-reynaud@hotmail.fr">mickael-reynaud@hotmail.fr</td>
           </tr>
           <br>
           <tr>
              <th>Téléphone</th>
              <td>079 568 99 67</td>
           </tr>
           <tr>
              <th>Lieu d'origine et nationalité</th>
              <td>Farvany, Suisse</td>
           </tr>
           <tr>
              <th>Etat civil</th>
              <td>Célibataire</td>
           </tr>
           <tr>
              <th>Scolarité obligatoire</th>
              <td>2000-2011</td>
           </tr>
           <tr>
              <th align="left" valign="top">Formation</th>
              <td>Apprentissage d’automaticien avec maturité technique
                 <br>
                 à l’Ecole des métiers de Fribourg</td>
              </tr>
              <tr>
                 <th align="left" valign="top">Etudes supérieurs</th>
                 <td>Première année à l’Ecole d’ingénieur de Fribourg
                    <br>
                    en section Télécommunication</td>
                 </tr>
                 <tr>
                    <th align="left" valign="top">Langues</th>
                    <td>Français
                       <br>
                       Anglais niveau B1
                       <br>
                       Allemand niveau B1, stage linguistique de 6 mois</td>
                    </tr>
                    <tr>
                       <th>Travail d'été</th>
                       <td>5 ans à l'école du Jura B pour des nettoyages</td>
                    </tr>
                    <tr>
                       <th>Loisirs</th>
                       <td>Aviron, cinéma, théâtre, montage vidéo</td>
                    </tr>
                 </table>
                 <ul>
                  <li class="annexe">En annexe lettre de motivation</li>
                  <li class="diplome">Diplômes</li>
               </ul>
            </body>
            <?php require_once('tpl/footer.php'); ?>
         </main>
      </body>
      </html>