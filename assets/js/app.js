$(document).foundation();

$('[data-done]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'done.php?id='+elt.data('done')
	})
	.done(function() {
		$(elt).parents('.tasklist-item').addClass('tasklist-item-close');
	})
});

$('[data-delete]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'delete.php?id='+elt.data('delete')
	})
	.done(function() {
		$(elt).parents('.tasklist-item').remove();
	})
});

$('[data-deleteuser]').click(function(e) {
	var elt = $(this);

	$.ajax({
		url: 'deleteuser.php?id='+elt.data('deleteuser')
	})
	.done(function() {
		$(elt).parents('.tasklist-item').remove();
	})
});
