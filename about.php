<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<?php require_once('tpl/head.php'); ?>
</head>
<body>
	<div>
		<?php require_once('tpl/header.php'); ?>
		<main class="container off-canvas-content" data-off-canvas-content>
			<div class="it">
				<article class="article">
					<p>
						My Task est une application web permettant à un groupe d’utilisateurs de gérer et
						s’attribuer différentes tâches entre eux.
					</p>
				</article>
				<article class="article">
					<p>
						Il s'agit d'un projet créé dans le cadre du cours d'icommunication à l'école d'ingénieur de Fribourg pour la section télécommunication en 2017
					</p>
				</article>
				<article class="article">
					<p>
						Pour faire ce site nous avons du utiliser toutes les technologies apprisent jusqu'à ce jour durant le cours. HTML, CSS, PHP, Jquery, gestion de bases de données SQL, responsive design avec et sans Foundation.
					</p>
				</article>
				<article class="article">
					<p>
						Il n'y a aucune installation requise vu qu'il s'agit d'un site web. Pour l'étape actuelle aucun plugin.
					</p>
				</article>
				<article class="article">
					<h1 class="art">index</h1>
					<p>
						Il s'agit de la page principale du site web.  le
						contenu du « main » et changé dynamiquement en fonction des paramètres de l’url. 
					</p>
				</article>
				<article class="article">
					<h1 class="art">update / edit</h1>
					<p>
						Update est utilisé pour beaucoup de chose car il gère ajout/modif pour les tâche et les utilisateurs. En fonction des paramètres passé par les formulaires.

						Edit est utilisé pour l'ajout et la modification de tâches.
					</p>
				</article>
				<article class="article">
					<h1 class="art">login</h1>
					<p>
						La gestion de mot de passe n’est pas sécurisée. Ils sont stockés en dur
						sur la base de données. Pour sécuriser il faut hasher le mot de passe sur la BD.
					</p>
				</article>
				<article class="article">
					<h1 class="art">tris</h1>
					<p>
						La gestion des tris est complexe car une requete SQL est construite sur la base
						des filtres actifs. Ce n’est pas juste un filtrage javascript. Je n'ai donc pas les compétences pour effectuer ce tris.
					</p>
				</article>
			</div>
			<?php require_once('tpl/footer.php'); ?>
		</main>
	</body>
	</html>